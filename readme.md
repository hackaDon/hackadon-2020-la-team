﻿## Hackadon - Pass'Sport pour l'Emploi
### Equipe la team

Techno utilisées :
- Php, Symfony
- Javascript
- HTML
- Css, Bootstrap

## Instruction d'installation

- Cloner le projet
`git clone https://framagit.org/hackaDon/hackadon-2020-la-team.git`
- Installer le CLI (Command Line Interface) de Symfony [https://symfony.com/download](https://symfony.com/download)
- Aller dans le projet et lancer le serveur avec la commande `symfony serve`

